### Will's _awesome_ AwesomeWM theme

If you want to use this, clone it, copy it to `/usr/share/awesome/themes` and edit it to suit; should work with either v3.5 or v4.0 of [AwesomeWM](http://awesomewm.org)
